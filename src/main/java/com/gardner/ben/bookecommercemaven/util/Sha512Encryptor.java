package com.gardner.ben.bookecommercemaven.util;

import com.gardner.ben.bookecommercemaven.util.qualifier.Sha512;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

@Sha512
public class Sha512Encryptor implements iEncrypt {

    @Override
    public String encrypt(String password, String salt) {
        ShaPasswordEncoder pe = new ShaPasswordEncoder(512);
            pe.setIterations(1024);
            String hash = pe.encodePassword(password, salt);

            return hash;
    }
    
}
