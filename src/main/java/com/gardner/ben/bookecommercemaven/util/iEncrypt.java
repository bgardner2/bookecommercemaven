/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gardner.ben.bookecommercemaven.util;

/**
 *
 * @author Ben
 */
public interface iEncrypt {
    public String encrypt(String password, String salt);
}
