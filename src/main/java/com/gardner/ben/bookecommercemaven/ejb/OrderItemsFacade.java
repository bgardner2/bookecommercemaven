package com.gardner.ben.bookecommercemaven.ejb;

import com.gardner.ben.bookecommercemaven.entity.OrderItems;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class OrderItemsFacade extends AbstractFacade<OrderItems> {
    @PersistenceContext(unitName = "com.gardner.ben_BookEcommerceMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrderItemsFacade() {
        super(OrderItems.class);
    }
    
}
