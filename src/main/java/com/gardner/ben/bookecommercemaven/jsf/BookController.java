package com.gardner.ben.bookecommercemaven.jsf;

import com.gardner.ben.bookecommercemaven.entity.Book;
import com.gardner.ben.bookecommercemaven.jsf.util.JsfUtil;
import com.gardner.ben.bookecommercemaven.jsf.util.PaginationHelper;
import com.gardner.ben.bookecommercemaven.ejb.BookFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.print.attribute.standard.Severity;
import javax.servlet.http.HttpSession;
import org.primefaces.event.CellEditEvent;

@Named("bookController")
@SessionScoped
public class BookController implements Serializable {


    private Book bookToAdd=null;
    private Book currentBook;
    private List<Book> books;
    private List<Book> selectedBooks;
    private List<Book> filteredBooks;


    
    
    @EJB
    private com.gardner.ben.bookecommercemaven.ejb.BookFacade BookFacade;
    
    @PostConstruct
    public void populateBooks() {
        bookToAdd = new Book();
        currentBook = new Book();
        books = this.getBookFacade().findAll();
       

    }

    public Book getCurrentBook() {
        return currentBook;
    }

    public void setCurrentBook(Book currentBook) {
        this.currentBook = currentBook;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<Book> getSelectedBooks() {
        return selectedBooks;
    }

    public void setSelectedBooks(List<Book> selectedBooks) {
        this.selectedBooks = selectedBooks;
    }

    public List<Book> getFilteredBooks() {
        return filteredBooks;
    }

    public void setFilteredBooks(List<Book> filteredBooks) {
        this.filteredBooks = filteredBooks;
    }
    
    

    public BookFacade getBookFacade() {
        return BookFacade;
    }

    public void setBookFacade(BookFacade BookFacade) {
        this.BookFacade = BookFacade;
    }
    
    public Book getBookToAdd() {
        return bookToAdd;
    }

    public void setBookToAdd(Book bookToAdd) {
        this.bookToAdd = bookToAdd;
    }
    
    
    public void showGrowl(){
        FacesContext context = FacesContext.getCurrentInstance();
        if(selectedBooks == null || selectedBooks.size() == 0){
            return; 
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Books added", selectedBooks.size() + 
                " books have been added to your cart."));
    }
    
    public void processAddToCart(){
        FacesContext context = FacesContext.getCurrentInstance();
        if(selectedBooks == null || selectedBooks.size() == 0){
            return; 
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Books added", selectedBooks.size() + 
                " books have been added to your cart."));
        
        //selectedBooks=null;
        
        
    }
    
    public void addBook(){
        this.getBookFacade().create(bookToAdd);
        this.setBookToAdd(new Book());
        books = this.getBookFacade().findAll();
    }

    
    
   
    
    

    

}
