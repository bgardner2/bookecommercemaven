/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gardner.ben.bookecommercemaven.jsf;

import com.gardner.ben.bookecommercemaven.ejb.BookFacade;
import com.gardner.ben.bookecommercemaven.ejb.BookOrderItemsFacade;
import com.gardner.ben.bookecommercemaven.ejb.OrderItemsFacade;
import com.gardner.ben.bookecommercemaven.ejb.OrdersFacade;
import com.gardner.ben.bookecommercemaven.entity.Book;
import com.gardner.ben.bookecommercemaven.entity.BookOrderItemsDTO;
import com.gardner.ben.bookecommercemaven.entity.OrderItems;
import com.gardner.ben.bookecommercemaven.entity.OrderItemsPK;
import com.gardner.ben.bookecommercemaven.entity.Orders;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Ben
 */
@Named(value = "cartController")
@SessionScoped
public class CartController implements Serializable {

    private Orders newOrder;

    private List<Book> booksInCart;
    private List<OrderItems> orderItems;
    private List<BookOrderItemsDTO> orderLineDetails;

    @Inject
    private BookController bookController;
    @Inject
    private OrdersFacade orderEAO;
    @Inject
    private OrderItemsFacade orderItemsEAO;
    @Inject
    private BookOrderItemsFacade bookOrderItemsEAO;

//    @Inject
//    private BookFacade bookEAO;
    /**
     * Creates a new instance of CartController
     */
    public CartController() {
    }

    /**
     * Creates a new order with the server's Date to insert order items into
     */
    private void createNewOrder() {
        newOrder = new Orders();
        newOrder.setOrderDate(new Date());
        newOrder = getOrderEAO().createAndReturn(newOrder);
    }

    /**
     * Prepares a list of order items to be inserted into the database,
     * but it does not associate an order with an order item record in the database.
     * To do so cal
     */
    public void initializeOrderItems() {
        if (booksInCart != null) {
//            OrderItemsPK opk;
//            int lineItemNumber = 1;
            BookOrderItemsDTO itemLine;
            orderLineDetails = new LinkedList<BookOrderItemsDTO>();

            for (Book b : booksInCart) {

//                opk = new OrderItemsPK();
//                opk.setOrderItemId(lineItemNumber);

//                itemLine = new BookOrderItemsDTO(opk);
                itemLine = new BookOrderItemsDTO();
                itemLine.setBook(b);

                itemLine.setQuantity(1);

                orderLineDetails.add(itemLine);
//                lineItemNumber++;
            }
        }
    }
    
    private void addOrderIdToOrderItems(){
        if(newOrder != null){
            OrderItemsPK opk;
            int lineItemNumber = 1;
            for(BookOrderItemsDTO boi : orderLineDetails){
                opk = new OrderItemsPK(newOrder.getOrderId(), lineItemNumber);
                boi.setOrderItemsPK(opk);
                lineItemNumber++;
            }
        }
    }

    private void saveOrderItems() {
        for (BookOrderItemsDTO boi : orderLineDetails) {
            getBookOrderItemsEAO().create(boi);
        }
    }

    private void resetVariables() {
        newOrder = null;
        orderItems = null;
        booksInCart = null;
    }

    public String processCart() {
        this.createNewOrder();
//        this.prepareOrderItems();
        this.addOrderIdToOrderItems();
        this.saveOrderItems();
        this.resetVariables();
        bookController.setSelectedBooks(null);
        return "book/ViewBooks";

    }

    

    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public Orders getNewOrder() {
        return newOrder;
    }

    public void setNewOrder(Orders newOrder) {
        this.newOrder = newOrder;
    }

//    public OrderItems getCurrentOrderItem() {
//        return currentOrderItem;
//    }
//    
//    public void setCurrentOrderItem(OrderItems currentOrderItem) {
//        this.currentOrderItem = currentOrderItem;
//    }
//    
//    public Book getCurrentBook() {
//        return currentBook;
//    }
//    
//    public void setCurrentBook(Book currentBook) {
//        this.currentBook = currentBook;
//    }
//    
//    public int getCurrentBookQuantity() {
//        return currentBookQuantity;
//    }
//    
//    public void setCurrentBookQuantity(int currentBookQuantity) {
//        this.currentBookQuantity = currentBookQuantity;
//    }
    public OrdersFacade getOrderEAO() {
        return orderEAO;
    }

    public void setOrderEAO(OrdersFacade orderEAO) {
        this.orderEAO = orderEAO;
    }

    public OrderItemsFacade getOrderItemsEAO() {
        return orderItemsEAO;
    }

    public void setOrderItemsEAO(OrderItemsFacade orderItemsEAO) {
        this.orderItemsEAO = orderItemsEAO;
    }

//    public BookFacade getBookEAO() {
//        return bookEAO;
//    }
//    
//    public void setBookEAO(BookFacade bookEAO) {
//        this.bookEAO = bookEAO;
//    }
    public List<Book> getBooksInCart() {
        return booksInCart;
    }

    public void setBooksInCart(List<Book> booksInCart) {
        this.booksInCart = booksInCart;
    }

    public List<OrderItems> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItems> orderItems) {
        this.orderItems = orderItems;
    }

    public List<BookOrderItemsDTO> getOrderLineDetails() {
        return orderLineDetails;
    }

    public void setOrderLineDetails(List<BookOrderItemsDTO> orderLineDetails) {
        this.orderLineDetails = orderLineDetails;
    }

    public BookOrderItemsFacade getBookOrderItemsEAO() {
        return bookOrderItemsEAO;
    }

    public void setBookOrderItemsEAO(BookOrderItemsFacade bookOrderItemsEAO) {
        this.bookOrderItemsEAO = bookOrderItemsEAO;
    }
//</editor-fold>

}
