package com.gardner.ben.bookecommercemaven.jsf;

import com.gardner.ben.bookecommercemaven.entity.Orders;
import com.gardner.ben.bookecommercemaven.ejb.OrdersFacade;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import org.primefaces.component.rowexpansion.RowExpansion;
import org.primefaces.event.ToggleEvent;


@Named("ordersController")
@RequestScoped
public class OrdersController implements Serializable {
    
    private Orders currentOrder;
    private List<Orders> orders = null;
    @EJB
    private com.gardner.ben.bookecommercemaven.ejb.OrdersFacade ejbFacade;
    
    public OrdersController() {
    }
    
    @PostConstruct
    public void populateOrders() {
        currentOrder = new Orders();
        orders = getEjbFacade().findAll();
        
    }

    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public Orders getCurrentOrder() {
        return currentOrder;
    }
    
    public void setCurrentOrder(Orders currentOrder) {
        this.currentOrder = currentOrder;
    }
    
    public List<Orders> getOrders() {
        return orders;
    }
    
    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }
    
    public OrdersFacade getEjbFacade() {
        return ejbFacade;
    }
    
    public void setEjbFacade(OrdersFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }
    
    
//</editor-fold>
    public void handleRowExpansion(ToggleEvent re ){
        //This variable isn't needed, it's just for checking what order ID was passed
        int orderid = ((Orders)re.getData()).getOrderId();
        
        currentOrder = (Orders)re.getData();
        
    }
}
