package com.gardner.ben.bookecommercemaven.jsf;

import com.gardner.ben.bookecommercemaven.ejb.BookOrderItemsFacade;
import com.gardner.ben.bookecommercemaven.entity.BookOrderItemsDTO;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named("bookOrderItemsController")
@SessionScoped
public class BookOrderItemsController implements Serializable {

    private BookOrderItemsDTO currentBook;
    private List<BookOrderItemsDTO> books;
    private List<BookOrderItemsDTO> selectedBooks;
    private List<BookOrderItemsDTO> filteredBooks;
    @Inject
    private OrdersController ordersController;
    
    
    @Inject
    private com.gardner.ben.bookecommercemaven.ejb.BookOrderItemsFacade bookOrderItemsFacade;
    
    @PostConstruct
    public void populateBooks() {
        currentBook = new BookOrderItemsDTO();
        books = this.getBookOrderItemsFacade().findAll();
       

    }

    public BookOrderItemsDTO getCurrentBook() {
        return currentBook;
    }

    public void setCurrentBook(BookOrderItemsDTO currentBook) {
        this.currentBook = currentBook;
    }

    public List<BookOrderItemsDTO> getBooks() {
        return books;
    }

    public void setBooks(List<BookOrderItemsDTO> books) {
        this.books = books;
    }

    public List<BookOrderItemsDTO> getSelectedBooks() {
        return selectedBooks;
    }

    public void setSelectedBooks(List<BookOrderItemsDTO> selectedBooks) {
        this.selectedBooks = selectedBooks;
    }

    public List<BookOrderItemsDTO> getFilteredBooks() {
        return filteredBooks;
    }

    public void setFilteredBooks(List<BookOrderItemsDTO> filteredBooks) {
        this.filteredBooks = filteredBooks;
    }
    
    

    public BookOrderItemsFacade getBookOrderItemsFacade() {
        return bookOrderItemsFacade;
    }

    public void setBookOrderItemsFacade(BookOrderItemsFacade orderItemsFacade) {
        this.bookOrderItemsFacade = orderItemsFacade;
    }
    
    public void showGrowl(){
        FacesContext context = FacesContext.getCurrentInstance();
        if(selectedBooks == null || selectedBooks.isEmpty()){
            return; 
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Books added", selectedBooks.size() + 
                " books have been added to your cart."));
    }
    
    public void processAddToCart(){
        FacesContext context = FacesContext.getCurrentInstance();
        if(selectedBooks == null || selectedBooks.isEmpty()){
            return; 
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Books added", selectedBooks.size() + 
                " books have been added to your cart."));
        
        selectedBooks=null;
        
    }
    
   
   
    
    

    

}
