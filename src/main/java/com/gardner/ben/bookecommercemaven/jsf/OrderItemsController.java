package com.gardner.ben.bookecommercemaven.jsf;

import com.gardner.ben.bookecommercemaven.entity.OrderItems;
import com.gardner.ben.bookecommercemaven.jsf.util.JsfUtil;
import com.gardner.ben.bookecommercemaven.jsf.util.PaginationHelper;
import com.gardner.ben.bookecommercemaven.ejb.OrderItemsFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

@Named("orderItemsController")
@SessionScoped
public class OrderItemsController implements Serializable {
    private List<OrderItems> orderItems;
    
    @Inject
    private OrderItemsFacade orderItemsFacade;

    @PostConstruct
    public void populateBooks(){
        orderItems = this.getOrderItemsFacade().findAll();
        
    }

    public OrderItemsController() {
    }

    public List<OrderItems> getCurrentOrderItems() {
        return orderItems;
    }

    public void setCurrentOrderItems(List<OrderItems> orderItems) {
        this.orderItems = orderItems;
    }
    
    public OrderItemsFacade getOrderItemsFacade() {
        return orderItemsFacade;
    }

    public void setOrderItemsFacade(OrderItemsFacade orderItemsFacade) {
        this.orderItemsFacade = orderItemsFacade;
    }
    
    
    
    

}
