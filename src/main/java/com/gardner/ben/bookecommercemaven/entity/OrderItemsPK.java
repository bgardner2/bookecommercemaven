package com.gardner.ben.bookecommercemaven.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class OrderItemsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "order_id", table = "order_items")
    private int orderId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "order_item_id", table = "order_items")
    private int orderItemId;

    public OrderItemsPK() {
    }

    public OrderItemsPK(int orderId, int orderItemId) {
        this.orderId = orderId;
        this.orderItemId = orderItemId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) orderId;
        hash += (int) orderItemId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderItemsPK)) {
            return false;
        }
        OrderItemsPK other = (OrderItemsPK) object;
        if (this.orderId != other.orderId) {
            return false;
        }
        if (this.orderItemId != other.orderItemId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gardner.ben.bookecommercemaven.entity.OrderItemsPK[ orderId=" + orderId + ", orderItemId=" + orderItemId + " ]";
    }
    
}
