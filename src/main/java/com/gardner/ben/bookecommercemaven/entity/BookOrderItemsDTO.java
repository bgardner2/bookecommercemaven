package com.gardner.ben.bookecommercemaven.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "order_items")
//@SecondaryTable(name = "book", pkJoinColumns = @PrimaryKeyJoinColumn(name = "book_id", referencedColumnName = "book_id"))
public class BookOrderItemsDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrderItemsPK orderItemsPK;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "book_id", table="order_items")
//    private int bookId;
    @Column(name = "quantity", table="order_items")
    private Integer quantity;
    @OneToOne
    @JoinColumn(name = "book_id", referencedColumnName = "book_id")
    private Book book;
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 140)
//    @Column(table = "book", name = "book_name", insertable = false, updatable = false )
//    private String bookName;
//    @Basic(optional = false)
//    @NotNull
//    @Column(table = "book", name = "book_price", insertable = false, updatable = false)
//    private double bookPrice;
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 200)
//    @Column(table = "book", name = "book_image_url", insertable = false, updatable = false)
//    private String bookImageUrl;
    @JoinColumn(name = "order_id", referencedColumnName = "order_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orders orders;

    public BookOrderItemsDTO() {
        this.quantity = 1;
    }

    public BookOrderItemsDTO(OrderItemsPK orderItemsPK) {
        this.orderItemsPK = orderItemsPK;
        this.quantity = 1;
    }

    public OrderItemsPK getOrderItemsPK() {
        return orderItemsPK;
    }

    public void setOrderItemsPK(OrderItemsPK orderItemsPK) {
        this.orderItemsPK = orderItemsPK;
    }

//    public int getBookId() {
//        return bookId;
//    }
//
//    public void setBookId(int bookId) {
//        this.bookId = bookId;
//    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

//    public String getBookName() {
//        return bookName;
//    }
//
//    public void setBookName(String bookName) {
//        this.bookName = bookName;
//    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

//    public double getBookPrice() {
//        return bookPrice;
//    }
//
//    public void setBookPrice(double bookPrice) {
//        this.bookPrice = bookPrice;
//    }
//
//    public String getBookImageUrl() {
//        return bookImageUrl;
//    }
//
//    public void setBookImageUrl(String bookImageUrl) {
//        this.bookImageUrl = bookImageUrl;
//    }
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.orderItemsPK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookOrderItemsDTO other = (BookOrderItemsDTO) obj;
        if (!Objects.equals(this.orderItemsPK, other.orderItemsPK)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BookOrderItems{" + "orderItemsPK=" + orderItemsPK + '}';
    }
    
    
    
}
