package com.gardner.ben.bookecommercemaven.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "order_items")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderItems.findAll", query = "SELECT o FROM OrderItems o"),
    @NamedQuery(name = "OrderItems.findOrdersAndBooks", query = "SELECT o.orderItemsPK, o.quantity, o.bookId, b.bookName, b.bookPrice FROM OrderItems o, Book b where o.bookId = b.bookId"),
    @NamedQuery(name = "OrderItems.findByOrderId", query = "SELECT o FROM OrderItems o WHERE o.orderItemsPK.orderId = :orderId"),
    @NamedQuery(name = "OrderItems.findByOrderItemId", query = "SELECT o FROM OrderItems o WHERE o.orderItemsPK.orderItemId = :orderItemId"),
    @NamedQuery(name = "OrderItems.findByBookId", query = "SELECT o FROM OrderItems o WHERE o.bookId = :bookId"),
    @NamedQuery(name = "OrderItems.findByQuantity", query = "SELECT o FROM OrderItems o WHERE o.quantity = :quantity")})
public class OrderItems implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrderItemsPK orderItemsPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "book_id")
    private int bookId;
    @Column(name = "quantity")
    private Integer quantity;
    @JoinColumn(name = "order_id", referencedColumnName = "order_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orders orders;

    public OrderItems() {
    }

    public OrderItems(OrderItemsPK orderItemsPK) {
        this.orderItemsPK = orderItemsPK;
    }

    public OrderItems(OrderItemsPK orderItemsPK, int bookId) {
        this.orderItemsPK = orderItemsPK;
        this.bookId = bookId;
    }

    public OrderItems(int orderId, int orderItemId) {
        this.orderItemsPK = new OrderItemsPK(orderId, orderItemId);
    }

    public OrderItemsPK getOrderItemsPK() {
        return orderItemsPK;
    }

    public void setOrderItemsPK(OrderItemsPK orderItemsPK) {
        this.orderItemsPK = orderItemsPK;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderItemsPK != null ? orderItemsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderItems)) {
            return false;
        }
        OrderItems other = (OrderItems) object;
        if ((this.orderItemsPK == null && other.orderItemsPK != null) || (this.orderItemsPK != null && !this.orderItemsPK.equals(other.orderItemsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gardner.ben.bookecommercemaven.entity.OrderItems[ orderItemsPK=" + orderItemsPK + " ]";
    }
    
}
